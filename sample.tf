resource "aws_instance" "hello" {
	ami = var.AMI
	instance_type = var.INSTANCE_TYPE
    subnet_id = aws_subnet.public.id
    vpc_security_group_ids = [aws_security_group.allow-ssh.id]
    associate_public_ip_address = true
    key_name="cicd"
}
