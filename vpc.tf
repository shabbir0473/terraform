resource "aws_vpc" "EKS" {
        cidr_block = var.CIDR
        instance_tenancy = "default"
        enable_dns_support = "false"
        enable_dns_hostnames = "false"
        tags = {
                Name = "EKS"
        }
}
