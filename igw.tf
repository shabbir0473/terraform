resource "aws_internet_gateway" "eksigw" {
	vpc_id = aws_vpc.EKS.id
	tags = { Name = "eksigw" }
}
