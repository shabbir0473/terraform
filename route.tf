resource "aws_route_table" "eks_route" {
	vpc_id = aws_vpc.EKS.id
	route {
		cidr_block = var.RCIDR
		gateway_id = aws_internet_gateway.eksigw.id
	}
	tags = { Name = "eks_route" }
}
resource "aws_route_table_association" "public_subnet_associatiation" {
	subnet_id = aws_subnet.public.id
	route_table_id = aws_route_table.eks_route.id
}
