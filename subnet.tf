resource "aws_subnet" "public" {
  vpc_id = aws_vpc.EKS.id
  availability_zone = "us-east-1a"
  cidr_block = var.SUB_CIDR
  map_public_ip_on_launch= "true"
  tags = { Name = "public-1a" }
}
resource "aws_subnet" "private" {
  vpc_id = aws_vpc.EKS.id
  availability_zone = "us-east-1b"
  cidr_block = var.SUB_CIDR2
  map_public_ip_on_launch= "false"
  tags = { Name = "private-1b" }
}
